<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'JudgmentController@index', function () {
    return view('home');
});

Route::get('/edit/{id}', 'JudgmentController@showJudgment');
Route::post('/edit/{id}', 'JudgmentController@editJudgment');

Route::post('/add-para', 'JudgmentController@addPara');
Route::post('/get-para', 'JudgmentController@getPara');
Route::post('/delete-para', 'JudgmentController@deletePara');
Route::post('/submit-para', 'JudgmentController@submitPara');
Route::post('/highlight-para', 'JudgmentController@highlightPara');
Route::get('/check/{id}','JudgmentController@checkData');

ALTER TABLE `judgment_details` ADD INDEX `idx_judement_details_year` (`CaseYear`);
ALTER TABLE `testanswer_details` ADD INDEX `idx_testanswer_details_CaseId` (`CaseId`);
ALTER TABLE `testanswer_details` ADD INDEX `idx_testanswer_details_User_Id` (`UserId`);
ALTER TABLE `testanswer_details` ADD INDEX `idx_testanswer_details_status` (`status`);
ALTER TABLE `judgment_details` ADD INDEX `idx_judement_details_seg_status` (`Seg_Status`);
ALTER TABLE `judgment_details` ADD INDEX `idx_judement_details_courtId` (`CourtId`);

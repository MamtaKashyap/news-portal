var ctxmenu = {
  container: null
  
  ,init: function() {
    this.hide();
    
    this.container = document.createElement('div');
    this.container.setAttribute('id', 'ctxmenu');
    this.container.className = 'ctxmenu hidden';
    
    document.body.appendChild(this.container);
  }
  
  ,set: function() {
    this.init();
    
    var settings = _.extend({
      content: null
      ,posX: 0
      ,posY: 0
    }, arguments[0] || {});

    this.container.innerHTML = settings.content;
    this.container.style.left = settings.posX + 'px';
    this.container.style.top = settings.posY + 'px';
  }
  
  ,show: function() {
    this.set(arguments[0] || {});

    this.container.className = 'ctxmenu';
  }
  
  ,hide: function() {
    if(this.container) {
      this.container.remove();
      this.container = null;
    }
  }
};

[].forEach.call(document.querySelectorAll('.with-ctxmenu'), function(el) {
  el.addEventListener('contextmenu', function(e) {
    // prevent the right click default action
    e.preventDefault();
    // prevent the auto kill
    e.stopPropagation();

    ctxmenu.show({
      content: document.getElementById('ctxmenu-tpl').innerHTML
      ,posX: e.clientX
      ,posY: e.clientY
    });
  });
});

// hide the contextmenu on left/right click
document.addEventListener('click', function() { ctxmenu.hide(); }, false);
document.addEventListener('contextmenu', function() { ctxmenu.hide(); }, false);
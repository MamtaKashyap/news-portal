<!DOCTYPE html>
<html lang="en">
<head>
  <title>Check Records</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
@include('tinyeditor');
</head>
<body>

<div class="container">

  <a href="#" onclick="submitData('{{Request::segment(2)}}');"><button class="btn btn-success" style="float:right; margin-bottom: 10px;">Submit Paragraphs</button></a>
  <a href="/edit/{{Request::segment(2)}}"><button class="btn btn-info" style="float:right; margin-right:20px; margin-bottom: 10px;">Back</button></a> 

  <h2>Judgments</h2>
  

  <table class="table table-bordered" style="width:50%; float: left;">
    <thead>
      <tr>
        <th>LQ</th>
      </tr>
    </thead>
    <tbody>
     @foreach($lqdata as $key=>$lq)
     <tr>
      <td>
         <p>Para: {{++$key}}<a href="#" style="float: right;" onclick="deletePara('{{$lq->id}}');"><img src="{{asset('images/delete.png')}}"></a></p>
     <textarea class="form-control" name="lqpara" id="{{$lq->id}}">{{$lq->content}}</textarea>
     </td>
     </tr>
     @endforeach
      </tbody>
  </table>


<table class="table table-bordered" style="width:50%; float: left;">
    <thead>
      <tr>
        <th>SCC</th>
      </tr>
    </thead>
    <tbody>

     @foreach($sccdata as $key=>$scc)
     <tr>
      <td>
         <p>Para: {{++$key}}<a href="#" style="float: right;" onclick="deletePara('{{$scc->id}}');"><img src="{{asset('images/delete.png')}}"></a></p>
     <textarea class="form-control" name="sccpara" id="{{$scc->id}}">{{$scc->content}}</textarea>
     </td> 
     </tr>
    @endforeach
      </tbody>
  </table>

</div>


<script>
  function deletePara(id)
  {
    if (confirm("Do you want to delete paragraph?"))
    {    
    $.ajax({
    type:'POST',
    url:'/delete-para',
    data:{'_token':'{{csrf_token()}}', id:id},
    success: function(data)
    {
      alert(data);
        location.reload(true);
    }
    });
  }
  }

  function submitData(id)
  {
    // var bb = $('[name="lqpara"]').attr("id");
    // alert(bb);
  

    // $('[name="lqpara"]').each(function(){
    // alert($(this).attr(id));
    // });
    // alert(taskArray);
    // alert(taskArray1);

    if (confirm("Do you want to submit paragraphs?"))
    {  
    var taskArray = new Array();
    var taskArray1 = new Array();
    var taskContent = new Array();
    var taskContent1 = new Array();

    $('[name="lqpara"]').each(function() {
       taskArray.push($(this).attr("id"));
       taskContent.push($(this).val());
      // alert($(this).attr("id"));
    });

    var taskArray1 = new Array();
    $('[name="sccpara"]').each(function() {
       taskArray1.push($(this).attr("id"));
       taskContent1.push($(this).val());
      // alert($(this).attr("id"));
    });

    $.ajax({
    type:'POST',
    url:'/submit-para',
    data:{'_token':'{{csrf_token()}}', id:id, taskArray:taskArray, taskArray1:taskArray1, taskContent:taskContent, taskContent1:taskContent1},
    success: function(data)
    {
      alert(data);
        location.reload(true);
    }
    });
  }
  }

</script>
</body>
</html>

<?php

namespace App\Http\Controllers;

use App\Judgment;
use Illuminate\Http\Request;
use DB;

class JudgmentController extends Controller
{
    //
    public function index()
    {
    	$data = Judgment::all();
    	return view('home',['data'=>$data]);

    }

    public function showJudgment(Request $request)
    {
    	$data = Judgment::find($request->id);
        // $getPara = DB::table('para')->where('judgment_id','=',$request->id)->max('para');
        // if($getPara==NULL)
        // {
        //     $getPara=1;
        // }
        // else
        // {
        //     $getPara = $getPara+1;
        // }
    	return view('edit',['data'=>$data]);
    }

    public function editJudgment(Request $request)
    {
    	$data = Judgment::find($request->id);
    	$save = $data->save();

    	if($save)
    	{
           $request->session()->flash('success','Record Saved');
    	}
    	else
    	{
    	   $request->session()->flash('error','Error in record saved.');
    	}

    }

    public function addPara(Request $request)
    {
        $judgment_id = $request->id;
        $para = $request->para;
        $content = $request->text;
        $type = $request->type;
        $data=array('judgment_id'=>$judgment_id,"para"=>$para,"content"=>$content,"type"=>$type);

        $save =  DB::table('para')->insert($data);

        if($save)
        {
            echo "Record Saved.";
        }
        else
        {
            echo "Error in Saved Record.";
        }

    }

    public function getPara(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $getPara = DB::table('para')->where('judgment_id','=',$id)->where('type','=',$type)->max('para');
        if($getPara==NULL)
        {
            $getPara=1;
        }
        else
        {
            $getPara = $getPara+1;
        }
        // return view('edit',['getPara'=>$getPara]);
        return response()->json(['getPara'=>$getPara]);  


    }

    public function checkData(Request $request)
    {
        $lqdata = DB::table('para')->where('judgment_id','=',$request->id)->where('type','=',1)->where('del','=',0)->get();
        $sccdata = DB::table('para')->where('judgment_id','=',$request->id)->where('type','=',2)->where('del','=',0)->get();
        
        return view('check',['lqdata'=>$lqdata,'sccdata'=>$sccdata]);
    }

    public function deletePara(Request $request)
    {
        //$delete = DB::table('para')->where('id','=',$request->id)->delete();
        $delete = DB::table('para')->where('id','=',$request->id)->update(['del'=>'1']);

        if($delete)
        {
            echo "Paragraph Deleted Successfully.";
        }
        else
        {
            echo "Error in Delete Paragraph.";
        }
    }

    public function submitPara(Request $request, Judgment $judgment)
    {
        // print_r($request->taskArray);
        // print_r($request->taskArray1);
        // print_r($request->taskContent);
        // print_r($request->taskContent1);

        $arr = array_combine($request->taskArray,$request->taskContent);
        $arr1 = array_combine($request->taskArray1,$request->taskContent1);
        // $arr2 = array_merge($arr,$arr1);
        // print_r($arr2);
        // die;
        print_r($arr);
        // print_r($arr1);
        die;
        foreach($arr as $key=>$val)
        {

         $update = DB::table('para')->where('judgment_id','=',$request->id)->where('id','=',$key)->update(["content"=>"$val"]);
        }

        if($update)
        {
            echo "Paragraphs Saved Successfully.";
        }
        else
        {
            echo "Error in save Paragraphs.";
        }
    }

    public function highlightPara(Request $request)
    {
        $id = $request->id;
        $html="";
        $getData = DB::table('para')->where('judgment_id','=',$id)->where('del','=',0)->get();
        foreach($getData as $data)
        {
            $html = $data->content." ";
        }
        return $html;

    }

   

}


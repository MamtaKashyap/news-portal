<!DOCTYPE html>
<html lang="en">
<head>
  <title>Judgment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
<style>
  .highlight {color:red;}
</style>
</head>
<body>
<div class="container">
  <h2>Judgments</h2>
<form style="text-align: center;" method="post">
  <input type="radio" name="type" value="1" checked> LQ
  <input type="radio" name="type" value="2"> SCC
</form> 
<a href="/check/{{Request::segment(2)}}"><button class="btn btn-success" style="float:right; margin-bottom: 10px;">Check Records</button></a>
<a href="/"><button class="btn btn-info" style="float:right; margin-right:20px; margin-bottom: 10px;">Back</button></a> 


  <table class="table table-bordered with-ctxmenu">
    <thead>
      <tr>
        <th>Judgment</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td id="paraText">{{$data->judgment}}</td>
        
      </tr>
      </tbody>
  </table>
</div>


<div class="hidden">
  <div id="ctxmenu-tpl">
    <ul class="ctxmenu-menu">
      <li>
        <a href="#" onclick="savePara('{{Request::segment(2)}}');">Paragraph <span id="getPara"></span></a>
      </li>
    </ul>

  </div>
</div>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://underscorejs.org/underscore-min.js'></script>
<script src="{{asset('js/script.js')}}"></script>
<script>
    $(document).ready(function(){
    var radioValue = $("input[name='type']:checked").val();

            if(radioValue){

                // alert("Your are a - " + radioValue);
                 $.ajax({
                 type:'POST',
                   url:'/get-para',
                   data:{'_token':'{{csrf_token()}}', 'id':'{{Request::segment(2)}}',type:radioValue},
                   success: function(data)
                   {
                      $.each(data, function(index, element) {
         
                      //alert(element);
                      $("#getPara").text(element);
                       
                   });
               
            }

        });

    }
        $("input[name='type']").click(function(){

            var radioValue = $("input[name='type']:checked").val();

            if(radioValue){

                // alert("Your are a - " + radioValue);
                 $.ajax({
                 type:'POST',
                   url:'/get-para',
                   data:{'_token':'{{csrf_token()}}', 'id':'{{Request::segment(2)}}',type:radioValue},
                   success: function(data)
                   {
                      $.each(data, function(index, element) {
         
                      //alert(element);
                      $("#getPara").text(element);
                       
                   });
               
            }

        });

    }
  });

        // var text = $("#paraText").text();
        // alert(text);

    // $.ajax({
    // type:'POST',
    // url:'/highlight-para',
    // data:{'_token':'{{csrf_token()}}', 'id':'{{Request::segment(2)}}'},
    // success: function(data)
    // {
    //   alert(data);
    //   var t = $("#paraText").text();
    //   t = t.replace("", "<span class='highlight'>"+data+"</span>");
    //   $("#paraText").html(t);
    // }
    // });

});

  function savePara(id)
  {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    }

    var type = $('input[name="type"]:checked').val();
    var para = $("#getPara").text();

     $.ajax({
    type:'POST',
    url:'/add-para',
    data:{'_token':'{{csrf_token()}}', id:id, text:text, para:para, type:type},
    success: function(data)
    {
      alert(data);
      location.reload(true);
    }
    });
    // alert(radio);
    // alert(text);
    // alert(id);
    // alert(para);
  }
  
</script>
</body>
</html>

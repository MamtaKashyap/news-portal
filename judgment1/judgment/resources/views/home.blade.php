<!DOCTYPE html>
<html lang="en">
<head>
  <title>Judgment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
  <h2>Judgments</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.no</th>
        <th>Judgment</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $key=>$val)
      <tr>
        <td>{{++$key}}</td>
        <td>{{substr($val->judgment,0,100)}}</td>
        <td><a href="edit/{{$val->id}}">Preview</a></td>
      </tr>
      @endforeach
      </tbody>
  </table>
</div>


</body>
</html>
